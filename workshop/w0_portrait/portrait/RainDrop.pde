class Drop {
  float x = random(width);
  float y = 0;
  float y_speed = 1;
  
  void fall() {
    y = y + y_speed;
  }
  
  void shiftX(float newX) {
    x = newX;
  }
  
  void show() {
    stroke(100, 80, 230);
    line(x, y, x, y+10);
  }
}
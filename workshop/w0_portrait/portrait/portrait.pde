/*
 * Interactive portrait1: mouse triggers frames
 *  - move mouse horizontally to display corresponding frame
 */

// load frames you saved in recorder
// (add more arrays for multiple sets of frames recorded to different folders)
PImage[] frames;
Drop [] drops = new Drop[100];

void setup() {
  // this is also the size of frame that's saved
  size(256, 256); 
  
  for(int i = 0; i < drops.length; i++) {
    drops[i] = new Drop();
  }
  
  // load frames (default is 30 frames in recorder data folder)
  frames = loadFrames("../recorder/data/", 30);
}

void draw() {
  // transform mouseX position into frame index
  //int i = floor(map(mouseX, 0, width - 1, 0, frames.length - 1));
  //image(frames[i], 0, 0);
  int indexInt =  floor(map(mouseX, 0, width - 1, 0, frames.length - 1));
    for (int i = 0; i < map(mouseX, 0, width, 100, 5000); i++) {
      int x = int(mouseX + (random(width/10) * random(-1, 1)));
      int y = int(mouseY + (random(height/10) * random(-1, 1)));
      color c = frames[indexInt].get(x, y);
     // c = Color(c[0] + mouseX, c[1] + mouseX, c[2] + mouseX);
      stroke(c);
      strokeWeight(5);
      point(x, y);
  }
  
   //drops[i].fall();
   //drops[i].show();

  // for making gif animations ...
  if (saveGif) {
    gifExport.setDelay(1);
    gifExport.addFrame();
  }
}

// load in the frames
// filename is 'frame-000.jpg', 'frame-001.jpg', ...
PImage[] loadFrames(String path, int n) {

  PImage[] f = new PImage[n];

  for (int i = 0; i < n; i++) {
    f[i] = loadImage(path + "frame-" + nf(i, 3) + ".jpg");
  } 
  return f;
}


// for making gifs

// https://github.com/01010101/GifAnimation
import gifAnimation.*; 
GifMaker gifExport;

boolean saveGif = false;

void keyPressed() {
  if (gifExport == null && !saveGif) {
    gifExport = new GifMaker(this, "me.gif");
    gifExport.setRepeat(0);
    saveGif = true;
    println("Start gif export");
  } else if (saveGif) {
    gifExport.finish();  
    saveGif = false;
    println("End gif export");
  }
}
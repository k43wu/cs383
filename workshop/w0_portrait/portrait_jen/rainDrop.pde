class rainDrop {
  float x = width/2;
  float y = 0;
  float y_speed = 1;
  
  void fall() {
    y = y + y_speed;
  }
  
  void show() {
    stroke(100, 80, 230);
    line(x, y, x, y+10);
  }
}
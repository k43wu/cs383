class NeuralAgent {
  int id;
  int timer;
  PVector origin;
  ArrayList<NeuralAgent> neighbours;
  ArrayList<Direction> neighboursDirs;
  
  boolean animating = false;
  boolean active = false;
  boolean animatingTill = false;
  boolean moving = false;
  boolean fallingAsleep = false;
  
  color agentColor;
  color fireColor;
  color sleepColor;
  
  float grayScale;
  float size;
  float step = 5; //For firing distance
  float fireLength = 40;
  float energy;
  float volume = 0.8;
  
  float stepAccumulate = 0;
  float tillAccumulate = 0;
  float posAccumulate = 0;
  
  NeuralAgent parent;
  NewPosition newPosition;
  NeuralAgent(int _id, PVector _origin, float _grayScale, float _size) {
    id = _id;
    neighbours = new ArrayList<NeuralAgent>();
    neighboursDirs = new ArrayList<Direction>();
    origin = _origin;
    grayScale = _grayScale;
    size = _size;
  }
  
  void addNeighbour(NeuralAgent agent, float distance) {
     neighbours.add(agent);
     Direction dir = new Direction(id, agent.id, origin.copy(), agent.origin.copy(), distance);
     neighboursDirs.add(dir);
  }
  
  void activeNodeTimerUpdate() {
    if (millis() - timer >= 15000) {
      active = false;
      timer = millis();
    }
  }
  
  void animatingSignal() {
    for(int i = 0; i < neighbours.size(); i++) {
      NeuralAgent neighbour = neighbours.get(i);
      if(neighbour != parent) {
        Direction dir =  neighboursDirs.get(i);
        if(dir.id == neighbour.id && dir.isAnimating) {
          if(stepAccumulate + step < dir.distance) {
            //Reaching neighbour, not there yet
            PVector Pd = PVector.add(origin,PVector.mult(dir.dirction,stepAccumulate));
            for (int j = 1; j <= fireLength; j++) {
              float inter = map(j, 1 , fireLength, 0, 1);
              color c = lerpColor(color(0,0,0), fireColor, inter);
              stroke(c);
              strokeWeight(5);
              PVector Ps_j = PVector.add(origin,PVector.mult(dir.dirction,max(stepAccumulate-fireLength+j, 0)));
              line(Ps_j.x, Ps_j.y, Ps_j.z, Pd.x, Pd.y, Pd.z);
            }
          } 
          else {
            //Reach neighbour!
            if(stepAccumulate < dir.distance) {
              PVector Pd = neighbour.origin;
              float offset = dir.distance - stepAccumulate;
              for (int j = 1; j <= offset; j++) {
                float inter = map(j, 1, offset, 0, 1);
                color c = lerpColor(color(0,0,0), fireColor, inter);
                stroke(c);
                strokeWeight(5);
                PVector Ps_j = PVector.add(origin,PVector.mult(dir.dirction,stepAccumulate+j));
                line(Ps_j.x, Ps_j.y, Ps_j.z, Pd.x, Pd.y, Pd.z);
             }
            }
            else {
              //Animate Till
              animatingTill = true;
              float offset = dir.distance - fireLength;
              float distance = fireLength - tillAccumulate;
              if(distance > 0) {
                 PVector Pd = neighbour.origin;
                 for (int j = 1; j <= distance; j++) {
                    float inter = map(j, 1, distance, 0, 1);
                    color c = lerpColor(color(0,0,0), fireColor, inter);
                    stroke(c);
                    strokeWeight(5);
                    PVector Ps_j = PVector.add(origin,PVector.mult(dir.dirction,offset+tillAccumulate+j));
                    line(Ps_j.x, Ps_j.y, Ps_j.z, Pd.x, Pd.y, Pd.z);
                 }
              } 
              else {
                //Finish this path, notify neibour to signal!
                 animatingTill = false;
                 dir.isAnimating = false;
                 neighbour.signalNotify(energy-50, this);
              }
            }
          }
        } 
      }
    }
    stepAccumulate+=step;
    if(animatingTill) {
      tillAccumulate += 4;
    }
    if(allFinishAnimating()) {
      //Finished firing animation, and reset properties
      animating = false;
      stepAccumulate = 0;
      this.energy = 0;
      this.parent = null;
    }
  }
  
  void signalNotify(float energy, NeuralAgent parent) {
    int parentCount = (parent == null) ? 0 : 1;
    if(parent != null) {
      float reduceColor = min(255-energy+20, 80);
      fireColor = color(red(agentColor)-reduceColor, green(agentColor)-reduceColor, blue(agentColor)-reduceColor);
    } else {
      fireColor = agentColor;
    }
    if(energy > 0 && (neighbours.size() > parentCount)) {
      this.energy = energy;
      this.parent = parent;
      this.animating = true;
      this.active = true;
      for(Direction direction : neighboursDirs){
        if((parent == null) || (direction.id != parent.id)){
          direction.isAnimating = true;
        }
      }
    } else {
      animating = false;
    }
  }
  
  void signalNotifyMoving() {
    if(newPosition != null) {
      moving = true;
      posAccumulate = 0;
    }
  }
  
  void animatingPosChange() {
    PVector Pd = origin;
    if(newPosition != null) {
      if(posAccumulate + step < newPosition.distance) {
        moving = true;
        Pd = PVector.add(origin,PVector.mult(newPosition.dirction, posAccumulate));
      } 
      else {
        Pd = newPosition.origin;
        newPosition.changePosition(origin, Pd);
        moving = false;
        origin = Pd;
        active = false;
      }
    } 
    pushMatrix();
    translate(Pd.x, Pd.y, Pd.z);
    fill(agentColor);
    noStroke();
    sphere(size);
    popMatrix();
    posAccumulate += step;
  }
  
  void signalNotifyGoSleep() {
    fallingAsleep = true; 
    sleepColor = agentColor;
  }
  
  void fallingAsleepAnimation() {
    float red = max(red(sleepColor)-2,40);
    float green = max(green(sleepColor)-2,40);
    float blue = max(blue(sleepColor)-2,40);
    sleepColor = color(red, green, blue);
    if(red == 40 && green == 40 && blue == 40) {
      fallingAsleep = false;
      active = false;
    }
  }
  
  
  boolean allFinishAnimating() {
    boolean done = true;
    for(Direction direction: neighboursDirs) {
      if(direction.isAnimating) {
        done = false;
        break;
      }
    }
    return done;
  }
}
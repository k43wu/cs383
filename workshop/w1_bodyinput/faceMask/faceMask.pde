/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

import processing.video.*;

import gab.opencv.*;

// to get Java Rectangle type
import java.awt.*; 
import org.opencv.imgproc.Imgproc;

Capture cam;
OpenCV opencv;
PImage myFace;
PImage happyFace;

// scale factor to downsample frame for processing 
float scale = 0.5;

// image to display
PImage output;
// the interactive drawing visualization
PGraphics viz;
Drop[] drops = new Drop[200];

// array of bounding boxes for face
Rectangle[] faces;

void setup() {
  size(640, 480);
  // want video frame and opencv proccessing to same size
   for (int i = 0; i < drops.length; i++) {
    drops[i] = new Drop();
  }
  cam = new Capture(this, int(640 * scale), int(480 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height);
  
  viz = createGraphics(width, height);
}


void draw() {
   
  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);

    // it's often useful to mirror image to make interaction easier
    opencv.flip(1);
    faces = opencv.detect();
    int ksize = 3;
    opencv.blur(ksize);
    int aperture = int(adjustX(2, 32)) * 2 - 1;
    // shows how to use "native" OpenCV calls
    Imgproc.medianBlur(opencv.getGray(), opencv.getGray(), aperture);

    // histogram equalization (like auto levels in photoshop)
    Imgproc.equalizeHist(opencv.getGray(), opencv.getGray());

     //threshold cutoff is between 0 and 255
    int cutoff = int(adjustY(0, 255));
    opencv.threshold(cutoff);
    opencv.close(5);
    output = opencv.getSnapshot(); 
  }

  // draw the image
  pushMatrix();
  scale(1 / scale);
  image(output, 0, 0);
  popMatrix();
  
  // draw face tracking debug
  viz.beginDraw();
  if (faces != null) {
    for (int i = 0; i < faces.length; i++) {
      // scale the tracked faces to canvas size
      float s = 1 / scale;
      int x = int(faces[i].x * s);
      int y = int(faces[i].y * s);
      int w = int(faces[i].width * s);
      int h = int(faces[i].height * s);
      // draw bounding box and a "face id"
      stroke(255, 255, 0);
      noFill();     
      rect(x, y, w, h);
      fill(255, 255, 0);
      text(i, x, y - 20);
      
      for (int j = 0; j < drops.length; j++) {
          drops[j].fall();
          if(drops[j].x < x || drops[j].x > (x + w)){
            float r  = random(0, 255);
            float g  = random(0, 255);
            float b  = random(0, 255);
            viz.stroke(r, g, b);
            float thick = map(drops[j].z, 0, 20, 1, 3);
            viz.strokeWeight(thick);
            viz.line(drops[j].x, drops[j].y, drops[j].x, drops[j].y+drops[j].len);
          }
      }
    }
  }
  viz.endDraw();
  image(viz, 0, 0);

  fill(255, 0, 0);
  text(nfc(frameRate, 1), 20, 20);
}

// helper functions to adjust values with mouse

float adjustX(float low, float high) {
   float v = map(mouseX, 0, width - 1, low, high); 
   println("adjustX: ", v);
   return v;
}

float adjustY(float low, float high) {
   float v = map(mouseY, 0, height - 1, low, high); 
   println("adjustY: ", v);
   return v;
}
class SmokeParticle {
  PVector pos;
  PVector vel;
  PVector acc;
  PImage texture;
  float opacity;
  float hue;
  
  SmokeParticle() {
    pos = new PVector(0,0);
  }
  SmokeParticle(PVector _v, PImage _texture) {
    pos = _v.copy();
    texture = _texture;
    
    float vx = randomGaussian() * 0.5;
    float vy = randomGaussian() * 0.5 - 1;
    vel = new PVector(vx, vy);
    acc = new PVector(0,0);
    opacity = 100;
  }
  
  void show() {
    update();
    render();
  }
  
  void update() {
    vel.add(acc.x, acc.y);
    pos.add(vel);
    opacity -= 2;
    acc.mult(0);
  }
  
  void render() {
    imageMode(CENTER);
    tint(baseHue,190,100, opacity);
    image(texture, pos.x, pos.y);
  }
  
  Boolean finshed() {
    if (opacity <= 0) {
      return true;
    } else {
      return false;
    }
  }
  void applyForce(PVector dir) {
    acc.add(dir);
  }
}
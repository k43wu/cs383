class SmokeAgent {
  ArrayList<SmokeParticle> particles;
  PVector origin;
  PImage texture;
  
  SmokeAgent(int _num, PVector _v, PImage _texture) {
    particles = new ArrayList<SmokeParticle>();
    origin = _v.copy();
    texture = _texture;
    for(int i = 0; i < _num; i++) {
      particles.add(new SmokeParticle());
    }
  }
  
  void addParticle() {
     PVector p = new PVector();
     p.x = origin.x + random(-10, 10);
     p.y = origin.y + random(-10, 10);
     particles.add(new SmokeParticle(p, texture));
  }
  
  void applyForce(PVector dir) {
    for(SmokeParticle p: particles) {
      p.applyForce(dir);
    }
  }
  
  void show() {
    for(int i = particles.size()-1; i >= 0; i--) {
      SmokeParticle particle= particles.get(i);
      if(!particle.finshed()) {
        particle.show();
      } else {
        //Delete this particle
        particles.remove(i);
      }
    }
  }
}
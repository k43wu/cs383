
Gui gui;
SmokeAgent sa;
ArrayList<SmokeAgent> textSmoke;
PrintWriter output;
ArrayList<PVector> pos;

float baseHue = 0;
int time;
int wait = 10000;

void setup() {
  gui = new Gui(this);
  gui.addSlider("baseHue", 0, 360);
  colorMode(HSB, 360, 100, 100, 100);
  
  pos = new ArrayList<PVector>();
  textSmoke = new ArrayList<SmokeAgent>();
  fullScreen();
  
  initPos();
  PImage texture = loadImage("smoke.png");
  for(PVector p: pos) {
    textSmoke.add(new SmokeAgent(0, p, texture));
  }
  time = millis();
}

void draw() {
  background(0);
  float dx = map(mouseX, 0, width, -0.2, 0.2);
  float dy = map(mouseY, 0, height, -0.2, -0.005);
  baseHue = (baseHue + 1) % 360;
 
  if(millis() - time >= 0 && millis() - time < 10000) {
    dx = 0.2;
    dy = -0.2;
  } else if (millis() - time >= 10000 && millis() - time < 20000) {
    dx = -0.2;
    dy = -0.005;
  } else {
    time = millis();
  }
  
  PVector wind = new PVector(dx, dy);
  
  for(SmokeAgent point: textSmoke) {
    point.applyForce(wind);
    point.show();
    //Keep adding new particles
    for(int i = 0; i < 2; i++) {
      point.addParticle();
    }
  }
}

//void mouseClicked() {
//  output.println(mouseX + "," + mouseY); // Write the coordinate to the file
//}

//void keyPressed() {
//   output.flush(); // Writes the remaining data to the file
//   output.close(); // Finishes the file
//   exit(); // Stops the program
//}

void initPos() {
  String[] lines = loadStrings("positions.txt");
  println("there are " + lines.length + " lines");
  for (int i = 0 ; i < lines.length; i++) {
    String line = lines[i];
    String[] loc = split(line, ',');
    //println(float(loc[0]) + "," + float(loc[1]));
    pos.add(new PVector(float(loc[0]), float(loc[1])+100));
  }
}
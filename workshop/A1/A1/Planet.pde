class Planet {
  
  float radius;
  float angle;
  float distance;
  float speed;
  String name;
  
  PVector rotate;
  PVector scale;
  PVector dirV;
  PVector v;
  
  PImage texture;
  PShape planet;
  
  Planet(float _distance, float _radius, float _speed, PVector _dir, String _textureURL, String _name) {
    
    radius = _radius;
    angle = random(TWO_PI);
    distance = _distance;
    speed = random(0.1, 0.25);
    texture = loadImage(_textureURL);
    name = _name;
    dirV = _dir;
    v = new PVector(dirV.x, dirV.y, dirV.z);
    v.mult(distance);
    
    rotate = new PVector(0, 0, 0);
    scale = new PVector(1, 1, 1);
    
    noStroke();
    noFill();
    planet = createShape(SPHERE, _radius);
    planet.setTexture(texture);
  }
  
  void setDistance(float _distance) {
    println(_distance);
    v.x = dirV.x;
    v.y = dirV.y;
    v.z = dirV.z;
    v.mult(_distance);
    //println(v);
  }
  
  void setV(float x, float y, float z) {
    v.x = x;
    v.y = y;
    v.z = z;
  }
  
  void setScale(float x, float y, float z) {
    scale.x = x;
    scale.y = y;
    scale.z = z;
  }
  
  void setRotate(float x, float y, float z) {
    rotate.x = x;
    rotate.y = y;
    rotate.z = z;
  }
  
  void setTexture(String _textureURL) {
    texture = loadImage(_textureURL);
  }
  
  void orbit() {
    angle = angle + speed;
  }
  
  void show() {
    // T * R * S * M
    pushMatrix();
    noStroke();
    PVector v2 = new PVector(1, 0, 1);
    PVector p = v.cross(v2);
    rotate(angle, p.x, p.y, p.z);
    translate(v.x, v.y, v.z);
    shape(planet);
    popMatrix();
  }
}
class Box {
  float dim = 100;
  PVector rotate;
  PVector scale;
  PVector translate;
  color c; 
  Box(float x, float y, float z) {
    translate = new PVector(x, y, z);
    rotate = new PVector(0, 0, 0);
    scale = new PVector(1, 1, 1);
    c = color(220, 150, 200);
  }
  
  void setTrans(float x, float y, float z) {
    translate.x = x;
    translate.y = y;
    translate.z = max(z, 30);
  }
  
  void setScale(float x, float y, float z) {
    scale.x = x;
    scale.y = y;
    scale.z = z;
  }
  
  void setRotate(float x, float y, float z) {
    rotate.x = x;
    rotate.y = y;
    rotate.z = z;
  }
  
  void setColor(float r, float g, float b) {
    c =  color (r, g, b);
  }
  
  void show() {
    // T * R * S * M
    pushMatrix();
    translate(translate.x+100, translate.y, translate.z); // translate and rotate 
    rotateX(rotate.x);
    rotateY(rotate.y); 
    rotateZ(rotate.z);
    scale(scale.x, scale.y, scale.z);
    fill(c);
    stroke(red(c)- 20, green(c)- 20, blue(c)- 20);
    box(dim);
    popMatrix();
  }
}
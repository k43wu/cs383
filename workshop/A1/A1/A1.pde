/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

import processing.video.*;

import gab.opencv.*;
import websockets.*;

// to get Java Rectangle type
import java.awt.*; 

WebsocketServer socket;
Capture cam;
OpenCV opencv;
// scale factor to downsample frame for processing 

PShape globe;
PShape news;
PShape art;


int mode = 0;
float scale = 0.5;
float rotate = 0;
int start;

ArrayList<Planet> planets;

Star[] stars = new Star[800];
float speed;

float text_angle = 0;
String [] text_string = new String[10];
int text_index = 0;

//Mode 3
PGraphics viz;
PImage drawingImg;
PImage drawDestImg;
int cellsize = 5;
int cols, rows;
int pointillize = 16;
boolean threeDMap = false;

// image to display
PImage output;
PImage img;
PImage star_bg;
PImage text_bg;
// array of bounding boxes for face
Rectangle[] faces;

void setup() {
  size(1152, 800, P3D);
  // want video frame and opencv proccessing to same size
  //socket = new WebsocketServer(this, 1337, "/p5websocket");
  cam = new Capture(this, int(1152 * scale), int(800 * scale));
  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
  cam.start();
  output = new PImage(cam.width, cam.height);
  
  star_bg = loadImage("texture/star.jpg");
  star_bg.resize(width,height);
  
  text_bg = loadImage("texture/text1.jpg");
  text_bg.resize(width,height);
  
  drawingImg = loadImage("texture/sunset.jpg");
  drawingImg.resize(width,height);
  
  initMode();
  initPlanets();
  initText();
  initDraw();
  viz = createGraphics(width, height);
}


void webSocketServerEvent(String msg){
 println(msg);
}

void draw() {
  if (cam.available() == true) {
    cam.read();
    // load frame into OpenCV 
    opencv.loadImage(cam);
    opencv.flip(1);
    faces = opencv.detect();
    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot();
  }
  if(mode == 0) {
    background(255);
    pushMatrix();
    scale(1 / scale);
    tint(255, 255);
    image(output, 0, 0);
    popMatrix();
    
    pushMatrix();
    translate( width/6, height/6, 50);
    rotateX(rotate+=0.1);
    rotateY(rotate+=0.05);
    shape(globe);
    popMatrix();
    
    pushMatrix();
    translate(width/2, height-200, 50);
    rotateX(rotate+=0.05);
    rotateY(rotate+=0.05);
    shape(news);
    popMatrix();
    
    pushMatrix();
    translate(width-200, height/5, 50);
    rotateX(rotate+=0.1);
    rotateY(rotate+=0.05);
    shape(art);
    popMatrix();
    
  }
  //else if(mode == 1) {
  //  drawStars();
  //}
  if (faces != null) {
    int faceCount = faces.length;
    if(faceCount > 0) {
      start = 0;
    }
    
    if(faceCount == 0 ) {
      //start = second();
      // println(start);
      //if(start > 10) {
      //  mode = 0;
      //}
      //if(mode == 1) {
      //  drawStars();
      //}
      //if(mode == 2) {
      //  background(237, 230, 218);
      //}
      mode = 0;
    }
    for (int i = 0; i < faceCount; i++) {
      start = 0;
      Rectangle face = faces[i];
      if(mode == 0) {
        float s = 1 / scale;
        int x = int(faces[i].x * s);
        int y = int(faces[i].y * s);
        int w = int(faces[i].width * s);
        int h = int(faces[i].height * s);
        stroke(255, 255, 0);
        noFill();     
        rect(x, y, w, h);
        
        if (checkStarHit(face)) {
          mode = 1;
        } 
        
        else if(checkTextHit((face))) {
          mode = 2;
        }
        
        else if(checkArtHit(face)) {
          mode = 3;
        }
      }
      
      else if (mode == 1) {
        drawStars();
        if(i == faceCount - 1) {
          drawPlanets(face);
        }
      }
      //Mode 2
      else if(mode == 2) {
        textFont(createFont("Georgia",40,true));
        textAlign(CENTER);
        smooth();
        drawTextFace(face);
      }
      
      else if(mode == 3) {
        background(0);
        float s = 1 / scale;
        int x = int(face.x * s);
        int y = int(face.y * s);
        int w = int(face.width * s);
        int h = int(face.height * s);
       
        float x_face = (2*x + w)/2;
        float y_face = (2*y + h)/2;
            
        if(threeDMap) {
          background(0);
          loadPixels();
          // Begin loop for columns
          for (int col = 0; col < cols; col++) {
            // Begin loop for rows
            for ( int row = 0; row < rows; row++) {
              int x_ = col*cellsize + cellsize/2; // x position
              int y_ = row*cellsize + cellsize/2; // y position
              int loc = x_ + y_ * drawDestImg.width;           // Pixel array location
              color c = drawDestImg.pixels[loc];       // Grab the color
              // Calculate a z position as a function of mouseX and pixel brightness
              float z_ = (w/(float)drawDestImg.width) * brightness(drawDestImg.pixels[loc]) * 8 - 50;
           
              // Translate to the location, set fill and stroke, and draw the rect
              pushMatrix();
              translate(x_,y_,z_);
              fill(red(c), green(c), blue(c), random(200, 255));
              noStroke();
              rectMode(CENTER);
              rect(0,0,cellsize,cellsize);
              popMatrix();
            }
          }
        } else {
            float x_loc = random(width);
            float y_loc = random(height);
            viz.beginDraw();
            for(int j = 0; j < 50; j++) { 
              float x_offset = random(-50,50);
              float y_offset = random(-50,50);
              color c = drawingImg.get(int(x_loc + x_offset), int(y_loc+y_offset));
              viz.fill(red(c), green(c), blue(c), random(30, 150));
              viz.noStroke();
              viz.ellipse(x_loc + x_offset, y_loc+ y_offset,25,25);
            }
            for(int j = 0; j < 80; j++) { 
              float x_offset = random(-80,80);
              float y_offset = random(-80,80);
              color c = drawingImg.get(int(x_face + x_offset), int(y_face+y_offset));
              viz.fill(red(c), green(c), blue(c), random(20, 255));
              viz.noStroke();
              viz.ellipse(x_face + x_offset, y_face+ y_offset,20,20);
            }
            viz.endDraw();
            image(viz, 0, 0);
        }
        
      }
    }
  }
}

void keyPressed() {
  if (key == '1') {
    mode = 1;
  } 
  else if (key == '2') {
    mode = 2;
  } 
  else if (key == '3') {
    mode = 3;
  }
  else if (key == ' ') {
    if(mode == 3) {
      threeDMap = !threeDMap;
      drawDestImg = viz.get();
    }
  }
  else {
    mode = 0;
  }
}

void initMode() {
  noStroke();
  noFill();
  globe = createShape(SPHERE, 50);
  globe.setTexture(loadImage("texture/m1.jpg"));
  
  news = createShape(SPHERE, 50);
  news.setTexture(loadImage("texture/m2.jpg"));
  
  art = createShape(SPHERE, 50);
  art.setTexture(drawingImg);
  
}
void initDraw() {
  cols = drawingImg.width/cellsize;
  rows = drawingImg.height/cellsize;
}
void initText() {
  String url = "http://www.cbc.ca/cmlink/rss-topstories";
  XML xml = loadXML(url);
  XML[] items = xml.getChild("channel").getChildren("item");
  int i = 0;
  for (XML x: items) {
    if(i >= 10) {
      break;
    }
    String title = x.getChild("title").getContent();
    text_string[i] = title;
    i++;
  }
}

void initPlanets() {
  for (int i = 0; i < stars.length; i++) {
    stars[i] = new Star();
  }
  
  planets = new ArrayList<Planet>();
  PVector merDir = new PVector(1,0.1,0);
  Planet mercury = new Planet(220, 18, 0.1, merDir, "texture/mercury.jpg", "mercury");
  planets.add(mercury);
  
  PVector venDir = new PVector(1,-0.2,0);
  Planet venus = new Planet(240, 25, 0.12, venDir, "texture/venus.jpg", "venus");
  planets.add(venus);
  
  PVector earthDir = new PVector(1, 0.1,0);
  Planet earth = new Planet(270, 28, 0.115, earthDir, "texture/earth.jpg", "earth");
  planets.add(earth);
  
  PVector marsDir = new PVector(1, 0.25, 0);
  Planet mars = new Planet(290, 25, 0.12, marsDir, "texture/mars.jpg", "mars");
  planets.add(mars);
  
  PVector juDir = new PVector(1, 0.1, 0);
  Planet jupiter = new Planet(340, 70, 0.1, juDir, "texture/jupiter.jpg", "jupiter");
  planets.add(jupiter);
  
  PVector satDir = new PVector(1, 0.13, 0);
  Planet saturn = new Planet(450, 40, 0.15, satDir,"texture/saturn.jpg", "saturn");
  planets.add(saturn);
  
  PVector urDir = new PVector(1, -0.1, 0);
  Planet uranus = new Planet(490, 20, 0.12, urDir, "texture/uranus.jpg", "uranus");
  planets.add(uranus);
 
  PVector nepDir = new PVector(1, 0.15, 0);
  Planet neptune = new Planet(520, 20, 0.126, nepDir, "texture/neptune.jpg", "neptune");
  planets.add(neptune);
  
  PVector pluDir = new PVector(0.8, 0.25, 0);
  Planet pluto = new Planet(540, 8, 0.1, pluDir, "texture/pluto.jpg", "pluto");
  planets.add(pluto);
}

Boolean checkBound(Rectangle face, float x2, float y2, float dim) {
  float s = 1 / scale;
  int x1 = int(face.x * s);
  int y1 = int(face.y * s);
  int w1 = int(face.width * s);
  int h1 = int(face.height * s);
  
  return x1 < x2 + dim && x1 + w1 > x2 && y1 < y2 + dim && y1 + h1 > y2;
}

Boolean checkStarHit(Rectangle face) {
  return checkBound(face, width/6, height/6, 100);
}

Boolean checkTextHit(Rectangle face) {
  return checkBound(face, width/2, height-200, 100);
}

Boolean checkArtHit(Rectangle face) {
  return checkBound(face, width-200, height/5, 100);
}

void drawStars() {
  int faceCount = faces.length;
  if(faceCount == 0) {
    speed = 0;
  }
  pushMatrix();
  background(star_bg);
  lights();
  translate(width/2, height/2);
  for (int i = 0; i < stars.length; i++) {
    stars[i].update();
    stars[i].show();
  }
  popMatrix();
  
}


void drawPlanets(Rectangle face) {
  float s = 1 / scale;
  int x = int(face.x * s);
  int y = int(face.y * s);
  int w = int(face.width * s);
  int h = int(face.height * s);
  speed = map(x, 0, width-w, 0, 50);
  pushMatrix();
  translate((2*x+w)/2, (2*y+h)/2);
  for(int j = 0; j < planets.size(); j++) {
    Planet planet = planets.get(j); 
    planet.orbit();
    planet.show();
  }
  popMatrix();
  pushMatrix();
  scale(1 / scale);
  PImage faceImg = output.get(face.x, face.y, face.width, face.height);
  tint(255,180);
  image(faceImg, face.x, face.y);
  popMatrix();
}

void drawTextFace(Rectangle face) {
  pushMatrix();
  background(237, 230, 218);
  scale(1 / scale);
  float s = 1 / scale;
  int x = int(face.x * s);
  int w = int(face.width * s);
  int y = int(face.y * s);
  int h = int(face.height * s);
  float threshold = map(x, 0, width-w, 0, 255);
  PImage faceImg = output.get(face.x, face.y, face.width, face.height);
  PImage destination = createImage(faceImg.width, faceImg.height, RGB);
  faceImg.loadPixels();
  destination.loadPixels();
  for(int x_loc = 0; x_loc < faceImg.width; x_loc++) {
    for (int y_loc = 0; y_loc < faceImg.height; y_loc++ )  {
      int loc = x_loc + y_loc * faceImg.width; 
      if(brightness(faceImg.pixels[loc]) <= threshold) {
        float r = random(0, 255);
        float g = random(0, 255);
        float b = random(0, 255);
        destination.pixels[loc]  = color(r,g,b);
      } else {
        destination.pixels[loc]  = color(237, 230, 218);
      }
    }
  }
  destination.updatePixels();
  image(destination,face.x,face.y);
  popMatrix();
  
  pushMatrix();
  translate((2*x+w)/2, (2*y+h)/2);
  text_angle += 0.1;
  float text_r = (w / 2) + 80;
  noFill();
  stroke(99, 96, 91);
  ellipse(0, 0, text_r*2, text_r*2);
  float arclength = 0;
  float rotate_speed = text_angle;
  int text_size = 40;
  float r_offset = 50;
  for(int lineCount = 0; lineCount < text_string.length; lineCount++) {
    if (lineCount % 2 != 0) {
      rotate_speed *= -1;
    }
    String line = text_string[lineCount];
    for (int i = 0; i < line.length(); i++) {
      char currentChar = line.charAt(i);
      float char_w = textWidth(currentChar);
      arclength += char_w/2;
      float theta = PI + arclength / text_r;
      theta += rotate_speed;
      pushMatrix();
      translate(text_r*cos(theta), text_r*sin(theta));
      rotate(theta+PI/2); // rotation is offset by 90 degrees
      fill(random(60,200), 96, 91, random(50,255));
      text(currentChar,0,0);
      popMatrix();
      arclength += char_w/2;
    }
    r_offset -= 2;
    text_r += r_offset;
    textSize(max(text_size -= 4, 8));
  }
  popMatrix();
}
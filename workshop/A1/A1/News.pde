class News {
  String url = "http://www.cbc.ca/cmlink/rss-topstories";
  String[] news;
  
  void setup() {
   XML xml = loadXML(url);
   XML[] items = xml.getChild("channel").getChildren("item");
   int i = 0;
   for (XML x: items) {
      // best to check if element is there using 
      // x.hasAttribute("cbc:type") and  x.getString("cbc:type")
      // but we'll just go for it and hope for the best
     
      String title = x.getChild("title").getContent();
      println(title);
      news[i] = title;
    }
  }
}
class Star {
  PVector position;
  float brightness;
  float brightness_orign;
  float radius;
  boolean dark = false;
  
  Star(float x, float y, float brightness) {
    position = new PVector(x, y);
    this.brightness = brightness;
    this.brightness_orign = brightness;
    float chance_size = random(0,1);
    
    if(chance_size < 0.9){
      radius = random(1,3);
    } else {
      radius = random(3,8);
    } 
  }
  
  void render(){
    float offset = 0;
    offset = random(2, brightness);
    if(!dark){
       brightness = min(brightness+0.5, brightness_orign);
    } else {
       brightness = max(brightness-1,10);
    }
    fill(brightness+offset);
    ellipse(position.x,position.y,radius,radius);
  }
  
  void setDark(boolean dark){
    this.dark = dark;
  }
}
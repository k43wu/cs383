Flock flock;
final float PADDING_WIDTH = 200;
final float PADDING_HEIGHT = 150;
final int WAIT_TIME_NORAIN = (int) (25 * 1000);
final int WAIT_TIME_RAIN = (int) (40 * 1000) + WAIT_TIME_NORAIN;

Flock flock_back;
Flock flock_back2;
Ripple ripple;

PImage bg;
ArrayList<Star> stars_front;
ArrayList<Star> stars_back;
float angle_front = 0;
float angle_back = 0;
float brightness = 255;
float start_time;
boolean isRaining = false;

void setup() {
  fullScreen();
  flock = new Flock();
  flock_back = new Flock();
  flock_back2 = new Flock();
  bg = loadImage("bg3.jpg");
  bg.resize(width,height);
  stars_front = new ArrayList<Star>();
  stars_back = new ArrayList<Star>();
  
  ripple = new Ripple();
  initStars();
  initFrontFlock();
  initBackFlocks();
  start_time = millis();
}

void draw() {
  tint(brightness);
  image(bg,0,0);
  
  angle_front += 0.1;
  angle_back -= 0.09;
  
  flock_back2.run();
  flock_back.run();
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angle_back));
  for(Star star: stars_back) {
    star.setDark(!isRaining);
    star.render();
  }
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angle_front));
  for(Star star: stars_front) {
    star.setDark(!isRaining);
    star.render();
  }
  popMatrix();
  
  flock.run();
  
  loadPixels();
  ripple.newframe(pixels);
  for (int loc = 0; loc < width * height; loc++) {
    pixels[loc] = ripple.col[loc];
  }
  updatePixels();
  checkRain();
}

void checkRain(){
  float time = millis() - start_time;
  if(time < WAIT_TIME_NORAIN) {
    brightness = max(brightness-2, 0);
    isRaining = false;
  }
  else if(time >= WAIT_TIME_NORAIN && time < WAIT_TIME_RAIN) {
   rain();
   brightness = min(brightness+2, 255);
   isRaining = true;
  }
  else {
    start_time = millis();
  }
}

void rain(){
  int x = int(random(50, width-50));
  int y = int(random(30, height-30));
  for (int j = y - ripple.riprad; j < y + ripple.riprad; j++) {
    for (int k = x - ripple.riprad; k < x + ripple.riprad; k++) {
      if (j >= 0 && j < height && k>= 0 && k < width) {
        ripple.ripplemap[ripple.oldind + (j * width) + k] += 512;
      }
    }
  } 
}

void initFrontFlock(){
  //Create some additional nice looking fishes I like:
   Fish blue_fish = new Fish(width/2,height/2, false);
   blue_fish.setFishColor(color(29,147,245));
   blue_fish.setFishScale_val(1.8);
   blue_fish.setFishHasPattern(false);
   flock.addFish(blue_fish);
   
   Fish purple_fish = new Fish(width/2,height/2, false);
   purple_fish.setFishColor(color(82,76,219));
   purple_fish.setFishScale_val(1.5);
   purple_fish.setFishHasPattern(true);
   flock.addFish(purple_fish);
   
   // Add an initial set of fishes into the system
  for (int i = 0; i < 15; i++) {
    Fish fish = new Fish(width/2,height/2, false);
    flock.addFish(fish);
  }
   
   Fish cyan_fish = new Fish(width/2,height/2, false);
   cyan_fish.setFishColor(color(49,197,195));
   cyan_fish.setFishScale_val(2);
   cyan_fish.setFishHasPattern(false);
   flock.addFish(cyan_fish);
   
   Fish red_fish = new Fish(width/2,height/2, false);
   red_fish.setFishColor(color(231,40,17));
   red_fish.setFishScale_val(1.7);
   red_fish.setFishHasPattern(false);
   red_fish.setFishHasPattern(true);
   flock.addFish(red_fish);
}

void initBackFlocks(){
  for(int i = 0; i < 40; i++){
    Fish fish = new Fish(width/2,height/2, true);
    flock_back.addFish(fish);
  }
  
  for(int i = 0; i < 30; i++){
    Fish fish = new Fish(width/2,height/2, true);
    flock_back2.addFish(fish);
  }
}

Star createStar(boolean front){
  float radius = (height/3);
  float brightness = random(120, 160);
  if(!front) {
    radius = (height/2);
    brightness = random(50, 90);
  }
  float a = random(0,1) * TWO_PI;
  float r = radius * sqrt(random(0,1));
  float x = cos(a) * r;
  float y = sin(a) * r;
  Star star = new Star(x, y, brightness);
  return star;
}

void initStars(){
  for(int i = 0; i < 300; i++) {
    stars_front.add(createStar(true));
  }
  for(int i = 0; i < 600; i++) {
    stars_back.add(createStar(false));
  }
}
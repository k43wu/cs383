class Flock {
  ArrayList<Fish> fishes; // An ArrayList for all the boids

  Flock() {
    fishes = new ArrayList<Fish>(); // Initialize the ArrayList
  }

  void run() {
    for (Fish fish : fishes) {
      fish.run(fishes);  // Passing the entire list of boids to each fish individually
    }
  }
  void addFish(Fish fish) {
    fishes.add(fish);
  }

}
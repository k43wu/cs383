class Fish {
  //========== fish position properties
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxforce;    // Maximum steering force
  float maxspeed;    // Maximum speed
  
  float sep_weight = random(0.8, 1.2);
  float ali_weight = random(0.5, 0.8);
  float coh_weight = random(0.4, 0.6);
  
  //========== sin wave movement
  int xspacing = 5;
  int wave_width;
  float wave_theta = 0.0;
  float amplitude = 5;  //height of the wave
  float period = 150;
  float dx;
  
  //========== fish appearence properties
  color fishColor;
  float fishScale;
  boolean fish_hasPattern = false;
  boolean fish_hasPath = false;
  boolean isDeep = false;
  
  //==========fish path
  ArrayList<PVector> history = new ArrayList<PVector>();
  
  Fish(float x, float y, boolean isDeep) {
    this.isDeep = isDeep;
    float chance_scale = random(0,1);
    float chance_pattern =  random(0,1);
    float chance_color = random(0,1);
    float chance_path = random(0,1);
    if(chance_pattern > 0.8) {
      this.fish_hasPattern = true;
    }
    if(!isDeep) {
      if(chance_path > 0.5) {
      this.fish_hasPath = true;
      }
    }
    
    setFlocking(x, y);
    setFishScale(chance_scale);
    setFishColor(chance_color);
  }
  
  void setFlocking(float x, float y){
    this.acceleration = new PVector(0,0);
    this.velocity = new PVector(random(-1,1),random(-1,1));
    this.position = new PVector(x,y);
    if(!isDeep){
      this.maxspeed = random(7,12);
      this.maxforce = random(0.1, 0.2);
    }
    else {
      this.maxspeed = random(2,5);
      this.maxforce = random(0.05, 0.1);
    }
    this.dx = (TWO_PI/period) * xspacing;
    this.wave_width = 60;
  }
  
  void setFishScale(float chance_scale) {
    if(!isDeep) {
     if(chance_scale < 0.8) {
        fishScale = random(0.5,0.6);
      } 
      else if (chance_scale >= 0.8 && chance_scale < 0.95){
        fishScale = random(0.7,1.0);
      } 
      else {
        fishScale = random(1.0,2.0);
      }
    } else {
      fishScale = random(0.15,0.4);
    }
  }
  
  void setFishColor(float chance_color) {
    float offset = random(0, 30);
    if(isDeep) {
      offset = random(120, 180);
    }
    if(chance_color < 0.6) {
      if(chance_color < 0.4) {
        fishColor = color(random(180,255)-offset, random(20,50)-offset,random(30,100)-offset);
      }
      else {
        fishColor = color(random(180,255)-offset,random(120,200)-offset,random(30,100)-offset);
      }
    }
    else if(chance_color >= 0.6 && chance_color < 0.8) {
       fishColor = color(random(80,120)-offset, random(120,250)-offset, random(30,50)-offset);
    }
    else if(chance_color >= 0.8 && chance_color <= 1) {
       fishColor = color(random(30,100)-offset,  random(100,200)-offset, random(180,255)-offset);
    } 
  }
  
  void setFishColor(color fishColor){
    this.fishColor = fishColor;
  }
  
  void setFishScale_val(float scale){
    this.fishScale = scale;
  }
  
  void setFishHasPattern(boolean hasPattern){
    this.fish_hasPattern = hasPattern;
  }
  
  
  void run(ArrayList<Fish> fishes) {
    flock(fishes);
    update();
    render();
  }
  
  void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }
  
    // We accumulate a new acceleration each time based on three rules
  void flock(ArrayList<Fish> fishes) {
    PVector sep = separate(fishes);   // Separation
    PVector ali = align(fishes);      // Alignment
    PVector coh = cohesion(fishes);   // Cohesion
    PVector boundaryForce = boundaries();   // Boundary check
    // Arbitrarily weight these forces
    sep.mult(sep_weight);
    ali.mult(ali_weight);
    coh.mult(coh_weight);
    // Add the force vectors to acceleration
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
    applyForce(boundaryForce);
  }
  
    // Method to update position
  void update() {
    velocity.add(acceleration);
    velocity.limit(maxspeed);
    position.add(velocity);
    acceleration.mult(0);
    
    if(fish_hasPath) {
      history.add(position.copy());
      if(history.size() > 30) {
        history.remove(0);
      }
    }
  }
  
    // A method that calculates and applies a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
  PVector seek(PVector target) {
    PVector desired = PVector.sub(target,position);  // A vector pointing from the position to the target
    // Normalize desired and scale to maximum speed
    desired.normalize();
    desired.mult(maxspeed);
    // Steering = Desired minus Velocity
    PVector steer = PVector.sub(desired,velocity);
    steer.limit(maxforce);  // Limit to maximum steering force
    return steer;
  }
  
  PVector boundaries() {
    float x = 0;
    float y = 0;
    if (position.x < PADDING_WIDTH) {
      x = PADDING_WIDTH - position.x;
    } 
    else if (position.x > width - PADDING_WIDTH) {
      x = (width - PADDING_WIDTH) - position.x;
    } 
    if (position.y < PADDING_HEIGHT) {
      y = PADDING_HEIGHT - position.y;
    } 
    else if (position.y > height-PADDING_HEIGHT) {
      y = (height - PADDING_HEIGHT) - position.y;
    }
    PVector desired = new PVector(x, y);
    float magnitude = desired.mag() / (PADDING_WIDTH);
    desired.setMag(magnitude);
    
    return desired;
  } 
  
 void render() { 
    float theta = velocity.heading2D();
    if(fish_hasPath) {
      pushMatrix();
      drawPath();
      popMatrix();
    }
    pushMatrix();
    translate(position.x,position.y);
    scale(fishScale);
    rotate(theta);
    drawFish();
    popMatrix();
  }
  
  void drawPath() {
    for(int i = 0; i < history.size(); i++) {
      pushMatrix();
      PVector pos = history.get(i);
      translate(pos.x,pos.y);
      scale(fishScale);
      noStroke();
      color pathColor = color(red(fishColor)+20, green(fishColor)+20, blue(fishColor)+20);
      fill(pathColor, i/5);
      int offset = 50-i;
      ellipse(0,0,10+offset,10+offset);
      popMatrix();
    }
  }
  
  void drawFish(){
    noStroke();
    wave_theta += 0.1;
    //for every x val, calculate the corresponding y val.
    float x = wave_theta;
    int count = wave_width/xspacing;
    int r = 45;
    int trans = 191;
    
    //Draw Fish Head;
    fill(fishColor, 230);
    ellipse(0, 0, 50, 30);
    
    fill(fishColor, 180);
    ellipse(-xspacing, 0, 30, 35);
    
    for (int i = 0; i <= count+2; i++) {
      fill(fishColor, trans);
      trans -= 10;
      r-=3;
      float y = (sin(x) * amplitude);
      if(i < count) {
        //Draw fish body
        x+=dx;
        if(i > 1) {
          if(fish_hasPattern) {
            int colorOffset = 10;
            if(i % 2 == 0) {
              colorOffset = -10;
            }
            fill(color(red(fishColor)+colorOffset, green(fishColor)+colorOffset, blue(fishColor)+colorOffset), 220);
          } 
          else {
            fill(fishColor, trans);
          }
           ellipse(-i*xspacing,y*(float)(Math.pow((float)i/count, 2)), r, r);
        }
      } 
      else if(i == count+2){
        //Draw Fishtail
        fill(fishColor, 150);
        pushMatrix();
        translate(-i * xspacing, y-10);
        rotate(PI/5.0);
        ellipse(0, 0, 40, 10);
        popMatrix();
        
        pushMatrix();
        translate(-i * xspacing, y+10);
        rotate(PI/-5.0);
        ellipse(0, 0, 40, 10);
        popMatrix();
      }
    }
    
    //Draw Fish Wing;
    fill(fishColor, 100);
    pushMatrix();
    translate(-count/5 * xspacing, -15);
    rotate(PI/5.0);
    ellipse(0, 0, 40, 10);
    popMatrix();
    
    pushMatrix();
    translate(-count/5 * xspacing, 15);
    rotate(PI/-5.0);
    ellipse(0, 0, 40, 10);
    popMatrix();
  }

  // Separation
  // Method checks for nearby boids and steers away
  PVector separate (ArrayList<Fish> fishes) {
    float desiredseparation = 25.0f;
    PVector steer = new PVector(0,0,0);
    int count = 0;
    // For every boid in the system, check if it's too close
    boolean hasNeighbour = false;
    for (Fish other : fishes) {
      float d = PVector.dist(position,other.position);
      // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
      if ((d > 0) && (d < desiredseparation)) {
        // Calculate vector pointing away from neighbor
        PVector diff = PVector.sub(position,other.position);
        diff.normalize();
        diff.div(d);        // Weight by distance
        steer.add(diff);
        count++;            // Keep track of how many
        hasNeighbour = true;
      }
    }
    
    // Average -- divide by how many
    if (count > 0) {
      steer.div((float)count);
    }
    // As long as the vector is greater than 0
    if (steer.mag() > 0) {
      // Implement Reynolds: Steering = Desired - Velocity
      steer.normalize();
      steer.mult(maxspeed);
      steer.sub(velocity);
      steer.limit(maxforce);
    }
    return steer;
  }
   // Alignment
  // For every nearby boid in the system, calculate the average velocity
  PVector align (ArrayList<Fish> fishes) {
    float neighbordist = 50;
    PVector sum = new PVector(0,0);
    int count = 0;
    for (Fish other : fishes) {
      float d = PVector.dist(position,other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.velocity);
        count++;
      }
    }
    if (count > 0) {
      sum.div((float)count);
      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum,velocity);
      steer.limit(maxforce);
      return steer;
    } else {
      return new PVector(0,0);
    }
  }

  // Cohesion
  // For the average position (i.e. center) of all nearby boids, calculate steering vector towards that position
  PVector cohesion (ArrayList<Fish> fishes) {
    float neighbordist = 50;
    PVector sum = new PVector(0,0);   // Start with empty vector to accumulate all positions
    int count = 0;
    for (Fish other : fishes) {
      float d = PVector.dist(position,other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.position); // Add position
        count++;
      }
    }
    if (count > 0) {
      sum.div(count);
      return seek(sum);  // Steer towards the position
    } else {
      return new PVector(0,0);
    }
  }

}
class Boid {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float r;
  float maxforce;    // Maximum steering force
  float maxspeed;    // Maximum speed
  
  //========== sin wave
  int xspacing = 5;
  int wave_width;
  float wave_theta = 0.0;
  float amplitude = 5;  //height of the wave
  float period = 100;
  float dx;
  
  //========== fish properties
  //float fishSize = random(50, 30);
  color fishColor;
  float fishScale;
  float fishScale_y;
  
  
  Boid(float x, float y) {
    acceleration = new PVector(0,0);
    velocity = new PVector(random(-1,1),random(-1,1));
    position = new PVector(x,y);
    maxspeed = random(6,2);
    maxforce = random(0.1, 0.05);
    dx = (TWO_PI/period) * xspacing;
    wave_width = 70;
    fishColor = color(random(0,255), random(0,255), random(0,255));
    float chance = random(0,1);
    if(chance < 0.8) {
      fishScale = random(0.3,0.6);
    } else if (chance >= 0.8 && chance < 0.9){
      fishScale = random(0.7,1.0);
    } else {
      fishScale = random(1.0,2.0);
    }
  }

  void run(ArrayList<Boid> boids) {
    flock(boids);
    update();
    render();
  }

  void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }

  // We accumulate a new acceleration each time based on three rules
  void flock(ArrayList<Boid> boids) {
    PVector sep = separate(boids);   // Separation
    PVector ali = align(boids);      // Alignment
    PVector coh = cohesion(boids);   // Cohesion
    PVector boundaryForce = boundaries();   // Boundary check
    // Arbitrarily weight these forces
    sep.mult(1.0);
    ali.mult(0.6);
    coh.mult(0.5);
    // Add the force vectors to acceleration
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
    applyForce(boundaryForce);
  }

  // Method to update position
  void update() {
    // Update velocity
    velocity.add(acceleration);
    // Limit speed
    velocity.limit(maxspeed);
    position.add(velocity);
    // Reset accelertion to 0 each cycle
    acceleration.mult(0);
  }

  // A method that calculates and applies a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
  PVector seek(PVector target) {
    PVector desired = PVector.sub(target,position);  // A vector pointing from the position to the target
    // Normalize desired and scale to maximum speed
    desired.normalize();
    desired.mult(maxspeed);
    // Steering = Desired minus Velocity
    PVector steer = PVector.sub(desired,velocity);
    steer.limit(maxforce);  // Limit to maximum steering force
    return steer;
  }
  
  PVector boundaries() {
    float x = 0;
    float y = 0;
    if (position.x < d_w) {
      x = d_w - position.x;
    } 
    else if (position.x > width -d_w) {
      x = (width - d_w) - position.x;
    } 
    if (position.y < d_h) {
      y = d_h - position.y;
    } 
    else if (position.y > height-d_h) {
      y = (height - d_h) - position.y;
    }
    PVector desired = new PVector(x, y);
    float magnitude = desired.mag() / (4 * d_w);
    magnitude = magnitude / 2; 
    desired.setMag(magnitude);
    return desired;
  }  
  
  void render() {
    // Draw a triangle rotated in the direction of velocity
    float theta = velocity.heading2D();
    fill(175);
    stroke(0);
    pushMatrix();
    translate(position.x,position.y);
    scale(fishScale);
    rotate(theta);
    drawSinWave();
    endShape();
    popMatrix();
  }
  
  void drawSinWave(){
    noStroke();
    wave_theta += 0.1;
    //for every x val, calculate the corresponding y val.
    float x = wave_theta;
    int count = wave_width/xspacing;
    int r = 45;
    int trans = 191;
    
    for (int i = 0; i <= count; i++) {
      fill(fishColor, trans);
      trans -= 15;
      r-=3;
      float y = sin(x) * amplitude;
      if(i == 0) {
      }
      if(i < count) {
        x+=dx;
        ellipse(-i*xspacing,y, r, r);
      } 
      else if(i == count){
        //Fish till
        fill(fishColor, 63);
        pushMatrix();
        translate(-count * xspacing, y-10);
        rotate(PI/5.0);
        ellipse(0, 0, 40, 10);
        popMatrix();
        
        pushMatrix();
        translate(-count * xspacing, y+10);
        rotate(PI/-5.0);
        ellipse(0, 0, 40, 10);
        popMatrix();
      }
    }
  }

  // Wraparound
  void borders() {
    if (position.x < -r) position.x = width+r;
    if (position.y < -r) position.y = height+r;
    if (position.x > width+r) position.x = -r;
    if (position.y > height+r) position.y = -r;
  }

  // Separation
  // Method checks for nearby boids and steers away
  PVector separate (ArrayList<Boid> boids) {
    float desiredseparation = 25.0f;
    PVector steer = new PVector(0,0,0);
    int count = 0;
    // For every boid in the system, check if it's too close
    boolean hasNeighbour = false;
    for (Boid other : boids) {
      float d = PVector.dist(position,other.position);
      // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
      if ((d > 0) && (d < desiredseparation)) {
        // Calculate vector pointing away from neighbor
        PVector diff = PVector.sub(position,other.position);
        diff.normalize();
        diff.div(d);        // Weight by distance
        steer.add(diff);
        count++;            // Keep track of how many
        hasNeighbour = true;
      }
    }
    if(!hasNeighbour) {
      steer = new PVector(1,1,0);
    }
    // Average -- divide by how many
    if (count > 0) {
      steer.div((float)count);
    }

    // As long as the vector is greater than 0
    if (steer.mag() > 0) {
      // Implement Reynolds: Steering = Desired - Velocity
      steer.normalize();
      steer.mult(maxspeed);
      steer.sub(velocity);
      steer.limit(maxforce);
    }
    return steer;
  }

  // Alignment
  // For every nearby boid in the system, calculate the average velocity
  PVector align (ArrayList<Boid> boids) {
    float neighbordist = 50;
    PVector sum = new PVector(0,0);
    int count = 0;
    for (Boid other : boids) {
      float d = PVector.dist(position,other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.velocity);
        count++;
      }
    }
    if (count > 0) {
      sum.div((float)count);
      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum,velocity);
      steer.limit(maxforce);
      return steer;
    } else {
      return new PVector(0,0);
    }
  }

  // Cohesion
  // For the average position (i.e. center) of all nearby boids, calculate steering vector towards that position
  PVector cohesion (ArrayList<Boid> boids) {
    float neighbordist = 50;
    PVector sum = new PVector(0,0);   // Start with empty vector to accumulate all positions
    int count = 0;
    for (Boid other : boids) {
      float d = PVector.dist(position,other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.position); // Add position
        count++;
      }
    }
    if (count > 0) {
      sum.div(count);
      return seek(sum);  // Steer towards the position
    } else {
      return new PVector(0,0);
    }
  }
}
class Direction {
  int pid;  //parent id
  int id;  // id
  PVector dirction; //Normalized dirction vector
  float distance;
  boolean isAnimating;
  
  Direction(int _pid, int _id, PVector pOrigin, PVector origin, float _distance) {
    pid = _pid;
    id = _id;
    distance = _distance;
    dirction = origin.sub(pOrigin);
    dirction = dirction.normalize();
    isAnimating = false;
  }
}
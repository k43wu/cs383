class Audio {
  // Sound file
  private AudioPlayer file;
  Sampler sample;
  AudioOutput out;
  Summer mix;
  Delay delay;
  LowPassSP lowPass;
  int soundTrigger;
  
  boolean isPlayingSound = false;
  
  Audio(Minim minim, String filepath) {
    // Load file
    file = minim.loadFile(filepath, 512);
    sample = new Sampler(filepath, 2, minim);
    mix = new Summer();
    out = minim.getLineOut();
    delay = new Delay(0.4, 0.5, true); //time, amplitude factor, feedback
    
    lowPass = new LowPassSP(0, file.sampleRate());
    file.addEffect(lowPass);
    lowPass.setFreq(20000);
    
    // patch
    sample.patch(mix);
    mix.patch(delay).patch(out);
  }
  void play(float volume) {
    file.rewind();
    file.setVolume(volume);
    file.play();
    soundTrigger = millis() + ceil(file.length()); 
    isPlayingSound = true;
  }
}
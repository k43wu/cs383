class NewPosition {
   PVector origin;
   PVector dirction;
   double distance;
   
   NewPosition(PVector _origin,  PVector _pOrigin, double _distance) {
     origin = _origin.copy();
     distance = _distance;
     dirction = PVector.sub(_origin,_pOrigin);
     dirction = dirction.normalize();
   }
   
   void changePosition(PVector _newOrigin, PVector _oldOrigin) {
     origin = _newOrigin.copy(); 
     //distance = _distance;
     dirction = PVector.sub(_newOrigin,_oldOrigin);
     dirction = dirction.normalize();
   }
}
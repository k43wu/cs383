PImage worldMap;
ArrayList<Path> airPaths;
float mapWidth, mapHeight;
float zoom = 1;

float cx = mercX(0);
float cy = mercY(0);

void setup() {
  size(1024, 512);
  cx = mercX(0);
  cy = mercY(0);
  airPaths = new ArrayList<Path>();
  worldMap = loadImage("earth.jpg");
  mapWidth = width;
  mapHeight = height;
  loadAirData();
}

void draw() {
  translate(width/2, height/2);
  imageMode(CENTER);
  image(worldMap, 0, 0, mapWidth, mapHeight);
  
  for(Path p: airPaths) {
    fill(180,120,120);
    strokeWeight(0.5);
    //noStroke();
    ellipse(p.sourceLoc.x, p.sourceLoc.y,2,2);
    
    fill(120,120,200);
    ellipse(p.destLoc.x, p.destLoc.y,2,2);
    
    noFill();
    //color c = color(random(0, 255), random(0, 255), random(0, 255), random(0.5, 1));
    stroke(random(0, 255), random(0, 255), random(0, 255));
    curve(p.controlPoint.x, p.controlPoint.y,p.sourceLoc.x, 
     p.sourceLoc.y,p.destLoc.x, p.destLoc.y,p.controlPoint.x, p.controlPoint.y);
  }
}

float mercX(float lon) {
  lon = radians(lon);
  float a = (256/PI) * pow(2, zoom);
  float b = lon + PI;
  return a * b;
}

float mercY(float lat) {
  lat = radians(lat);
  float a = (256 / PI) * pow(2, zoom);
  float b = tan(PI / 4 + lat / 2);
  float c = PI - log(b);
  return a * c;
}

void loadAirData() {
  String airportUrl = "https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat";
  String[] airports = loadStrings(airportUrl);
  int airportCount = airports.length;
  println(airports.length);
  
  String[] paths = loadStrings("path.txt");
  for(String path: paths) {
    String[] pathTokens = split(path, ',');
    int sourceIdx = max(int(pathTokens[3]) - 1, 0);
    int destIdx = max(int(pathTokens[5]) - 1, 0);
    
    if(sourceIdx < airportCount && destIdx < airportCount) {
      String[] sourceTokens = split(airports[sourceIdx], ',');
      float s_lat = float(sourceTokens[6]);
      float s_lon = float(sourceTokens[7]);
      float s_x = mercX(s_lon) - cx;  //lon
      float s_y = mercY(s_lat) - cy; //lat
   
      PVector sourceLoc = new PVector(s_x, s_y);
      
      String[] desTokens = split(airports[destIdx], ',');
      float d_lat = float(desTokens[6]);
      float d_lon = float(desTokens[7]);
      
      float d_x = mercX(d_lon) - cx; //Lat
      float d_y = mercY(d_lat) - cy;  //Lon
      PVector destLoc = new PVector(d_x, d_y);
      airPaths.add(new Path(sourceLoc, destLoc));
      println(sourceLoc + "," + destLoc);
    }
    
  }
  
}
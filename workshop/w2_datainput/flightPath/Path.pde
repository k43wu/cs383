class Path {
  PVector sourceLoc;
  PVector destLoc;
  PVector midPoint;
  PVector normal;
  PVector controlPoint;
  float dis;
  
  Path(PVector _sourceLoc, PVector _destLoc) {
    sourceLoc = _sourceLoc.copy();
    destLoc = _destLoc.copy();
    dis = dist(sourceLoc.x, sourceLoc.y, destLoc.x, destLoc.y);
    
    midPoint = new PVector((_sourceLoc.x + _destLoc.x) / 2, (_sourceLoc.y + _destLoc.y) / 2);
    float dx = (_destLoc.x - _sourceLoc.x);
    float dy = (_destLoc.y - _sourceLoc.y);
    
    if(dx < 0) {
      normal = new PVector(dy, -dx);
    } else {
      normal = new PVector(-dy, dx);
    }
   
    
    controlPoint = new PVector(midPoint.x + (normal.x*log10(dis)/2), 
    midPoint.y + (normal.y*log10(dis)/2));
    
  }
  float log10 (float x) {
  return (log(x) / log(10));
}
 
}